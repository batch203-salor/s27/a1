// 2 Model Booking System with Embedding

course {

	"id": "course1",
	"name" : "Learning MongoDB",
	"description" : "MongoDB Basics and Relationships",
	"price": 459,
	"slots": 20,
	"schedule": "Mon - Fri, 8:00AM-5:00PM",
	"instrcutor" : "John Doe",
	"isActive" : true,
	"enrollees": [
		{
			"id" : 1,
			"userId": "user1",
			"username": "john123",
			"isPaid" : true,
      "dateEnrolled" : ISODate("2022-01-15")
		}
	]
}
{
  "id": "course2",
	"name" : "Learning Node JS",
	"description" : "Node JS Basics and Express JS",
	"price": 999,
	"slots": 20,
	"schedule": "Mon - Fri, 6:00PM-10:00PM",
	"instrcutor" : "John Smith",
	"isActive" : true,
	"enrollees": [
		{
			"id" : 1,
			"userId": "user2",
			"username": "jane123",
			"isPaid" : true,
      "dateEnrolled" : ISODate("2022-01-17")
    }
	]
}



//3 Model Booking System with Referencing




course {
	"id": "course1",
	"name" : "Learning MongoDB",
	"description" : "MongoDB Basics and Relationships",
	"price": 459,
	"slots": 20,
	"schedule": "Mon - Fri, 8:00AM-5:00PM",
	"instrcutor" : "John Doe",
	"isActive" : true,
}
{
  "id": "course2",
	"name" : "Learning Node JS",
	"description" : "Node JS Basics and Express JS",
	"price": 999,
	"slots": 20,
	"schedule": "Mon - Fri, 6:00PM-10:00PM",
	"instrcutor" : "John Smith",
	"isActive" : true,
}

