//3 Model Booking System with Referencing




enrollment: 
{
	"id" : 1,
	"userId" : "user1",
	"userName" : "john123",
	"courseId" : "course1",
	"courseName" : "Learning MongoDB",
	"isPaid": true,
	"dateEnrolled" : ISODate("2022-01-15")
}
{
   "id" : 2,
	"userId" : "user2",
	"userName" : "jane123",
	"courseId" : "course2",
	"courseName" : "Learning Node JS",
	"isPaid": true,
	"dateEnrolled" : ISODate("2022-01-17")
}
